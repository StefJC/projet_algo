# Projet Algo

This graphical interface gives the user the possibility to perform Burrows Wheeler encoding/decoding and Huffman compression/decompression.  
Specific modules can perform Burrows Wheeler encoding followed by Huffman compression and the reverse.  
The user can save his/her work in each module.  


## Getting Started

The following files are needed to run the application:

```
Huffman.py
Burrows_Wheeler.py
GUI_under_pressure.py

```
To start the application, run :
```
python3 GUI_under_pressure.py

```
Follow the indications in the GUI. Error message will appear if the opened file is not valid. 

### Prerequisites

Project developed on Ubuntu 19.10  
Tested on Python 3.X  
This program requires the following library:  
```
tkinter

```

### Testing the scripts

If you want to test the Burrows Wheeler encoding/decoding program, run:
```
python3 Burrows_Wheeler.py
```

If you want to test the Huffman compression/decompression program, run:
```
python3 Huffman.py
```
The following file can be used to test Burrows Wheeler encoding/Huffman compression and Huffman decompression/Burrows Wheeler decoding:
```
test_bwt_huffman.py
```
Run it directly with:
```
python3 test_bwt_huffman.py
```
For each test, the user will be prompted to enter a string.

## Author

* **Stéphane Mancini**


## Acknowledgments

* Special thanks to the young Jedi of the Master for all his coding advices
* Many thanks to all the kittens from the Master1 DLAD ;-)
* Google is my friend
