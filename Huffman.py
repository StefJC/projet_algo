#!/usr/bin/env python3
# -*- coding: utf-8 -*-

'''This script allows the compression/decompression of an input sequence using
the huffman algorithm. The count_nucs function returns the frequency of each
character present in the sequence which is required for the Huffman compression.
The huffman_encoding function is intended to compress the input sequence while
the huffman_decoding function decompresses it'''


def count_nucs(sequence):
    '''By giving a string as an input, this function returns a dictionary
    containing each letter present in the string and its frequency'''
    dico_nucs = {}
    #check each letter in the string, if not present in the dictionary, includes
    #it as a key and gives 1 as a value, if already present, add 1 to the value
    for sequ in sequence:
        for nucs in range(len(sequ)):
            if sequ[nucs] not in dico_nucs:
                dico_nucs[sequ[nucs]] = 1
            else:
                dico_nucs[sequ[nucs]] += 1
    return dico_nucs


class Node:
    '''This class is used to construct the nodes of the binary tree with their
    branches and the binary value of the branches'''
    def __init__(self, value, letter="", left_branch = None, right_branch = None,\
    binary = ""):
        self.value = value
        self.letter = letter
        self.right_branch = right_branch
        self.left_branch = left_branch
        self.binary = binary

    def __repr__(self):
        return "%s - %s -- %s _ %s = %s" % (self.value, self.letter, \
        self.left_branch, self.right_branch, self.binary)

class HuffmanEncodingResult:
    '''This class retrieves from the huffman_encoding function, the binary
    sequence generated during compression and the final ascii compressed
    sequence. It also retrieves the elements generated during compression and
    which are essential to decode the ascii sequence '''
    def __init__(self, letter_code, nodes, huffman_bincode, ascii_code, added_zero):
        self.letter_code = letter_code
        self.nodes = nodes
        self.huffman_bincode = huffman_bincode
        self.ascii_code = ascii_code
        self.added_zero = added_zero


def huffman_encoding(dico_nucs, sequence):
    '''This function needs a string as an input and the dictionary generated with
    the count_nucs function which contains each character of the sequence and
    their frequency. The function returns not only the ascii-compressed sequence,
    but also the elements which must be recorded in order to have the capacity
    to uncompress the original sequence'''
    leaves = []
    nodes = []
    #create leaves
    for nuc, value in dico_nucs.items():
        leaves.append(Node(value, nuc))
    #create nodes (including leaves)
    while len(leaves) > 1:
        #sort leaves/nodes on their frequency
        leaves.sort(key = lambda x: x.value)
        #add 1 to the left branch and 0 to the right one before creating the parent
        #of the 2 nodes with the smallest values
        leaves[0].binary = leaves[0].binary + "1"
        leaves[1].binary = leaves[1].binary + "0"
        #Create the parent of the nodes with the smallest values and append to leaves
        leaves.append(Node(leaves[0].value + leaves[1].value, leaves[0].letter+leaves[1].letter, \
        left_branch = leaves[0].letter, right_branch = leaves[1].letter))
        #Add the 2 nodes with the smalles values to the list of nodes
        nodes = nodes + leaves[0:2]
        #Remove the 2 smallest nodes to avoid using them again in the next loop
        #At the end of the while loop, only the root node is left
        leaves = leaves[2:]
    #add the last node (root node) present in leaves to the list of nodes
    nodes = nodes + leaves

    #sort the nodes according to the number of letters of the node in order to
    #orientate the list of nodes from the leaves to the root
    nodes.sort(key = lambda x: len(x.letter))

    # build a dictionary including all the leaves and the binary values of the
    #branches pointing to them
    letter_code = {}
    for node in nodes:
        if node.left_branch is None and node.right_branch is None:
            letter_code[node.letter] = node.binary

    #Go through the binary tree from the leaves up to the root and insert the value
    #of the branch in the code of the corresponding letter
    build_binary = ""
    for letter, code in letter_code.items():
        for node in nodes:
            #go through the ordered nodes except the leaves and build the binary code
            if (node.left_branch is not None and node.right_branch is not None) \
            and (letter in node.left_branch or letter in node.right_branch):
                build_binary = node.binary + build_binary
            letter_code[letter] = build_binary + code
        build_binary = ""

    #write the sequence in binary
    huffman_bincode = []
    for nuc in sequence:
        if nuc in letter_code:
            huffman_bincode.append(letter_code[nuc])
    huffman_bincode = ''.join(huffman_bincode)

    #if encoded sequence not multiple of 8, add a string of 0 at the end in order to
    #get chunks of 8 bits for the conversion to ascii
    round_upper = int(len(huffman_bincode) / 8) + (len(huffman_bincode) % 8 > 0)
    added_zero = 8*round_upper - len(huffman_bincode) #information to keep for recoding
    eightbit_encoding = huffman_bincode + "0"*added_zero
    #get 8-bit chunks of the encoded sequence in a list
    eightbit_list = []
    while len(eightbit_encoding) > 0:
        eightbit_list.append(eightbit_encoding[0:8])
        eightbit_encoding = eightbit_encoding[8:]

    #convert binary 8-bit chunks into base-2 integers
    bin2basetwo = []
    for bit in eightbit_list:
        bin2basetwo.append(int(bit, 2))
    #convert base-2 integers into ascii to get the final compressed string
    basetwo2ascii = []
    for bit in bin2basetwo:
        basetwo2ascii.append(chr(bit))
    ascii_code = "".join(basetwo2ascii)
    return HuffmanEncodingResult(letter_code, nodes, huffman_bincode, ascii_code,\
    added_zero)

class HuffmanDecodingResult:
    '''This class retrieves from the huffman_decoding function, the original
    binary sequence following decompression of the ascii compressed sequence
    as well as the original sequence'''
    def __init__(self, original_binseq, uncompressed_string):
        self.original_binseq = original_binseq
        self.uncompressed_string = uncompressed_string


def huffman_decoding(ascii_code, added_zero, letter_code):
    '''This function allows the decompression of a sequence compressed with the
    Huffman algorithm. In order to work properly, this function needs as an input
    the ascii-compressed sequence, the binary code for each character of the
    sequence and the number of added zeros to be removed from the intermediate
    binary sequence generated during the compression/decompression process (see
    commentaries in the huffman_encoding function)'''
    #convert ascii characters into base-2 integers
    ascii2basetwo = []
    for bit in ascii_code:
        ascii2basetwo.append(ord(bit))
    #convert base-2 integers into binary to go back to the original binary string
    basetwo2bin = []
    for bit in range(len(ascii2basetwo)):
        basetwo2bin.append(bin(ascii2basetwo[bit]))
        basetwo2bin[bit] = basetwo2bin[bit][2:]
        if len(basetwo2bin[bit]) < 8:
            basetwo2bin[bit] = "0" * (8 - len(basetwo2bin[bit])) + basetwo2bin[bit]
    basetwo2bin = "".join(basetwo2bin)
    #remove the zeros added to the binary string to get a multiple of 8
    if added_zero != 0:
        original_binseq = basetwo2bin[:-added_zero]
    else:
        original_binseq = basetwo2bin
    #decode the binary string into the original uncompressed string
    uncompressed_string = ""
    bin_code = ""
    for digit in original_binseq:
        bin_code = bin_code + digit
        for letter, code in letter_code.items():
            if bin_code == code:
                uncompressed_string = uncompressed_string + letter
                bin_code = ""
    return HuffmanDecodingResult(original_binseq, uncompressed_string)


if __name__ == "__main__":
    input_sequence = input("enter your sequence: ")
    print("Input sequence: ", input_sequence)

    count = count_nucs(input_sequence)
    print("The frequency for each character is: ", count)
    compress = huffman_encoding(count, input_sequence)
    print("The binary code for the characters is: ", compress.letter_code)
    print("\nBinary tree for the Huffman compression:")
    for i in range(len(compress.nodes)):
        print(compress.nodes[i])
    print("\nThe binary sequence is:\n", compress.huffman_bincode)
    print("The compressed sequence is:\n", compress.ascii_code)

    decompress = huffman_decoding(compress.ascii_code, compress.added_zero, \
    compress.letter_code)
    print("The uncompressed binary sequence is:\n", decompress.original_binseq)
    print("The uncompressed sequence is:\n", decompress.uncompressed_string)
