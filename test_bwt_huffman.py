#!/usr/bin/env python3
# -*- coding: utf-8 -*-
'''This script prompts the user to enter a sequence and tests all the functions
from the Burrows-Wheeler and Huffman libraries'''

import Burrows_Wheeler as bw
import Huffman as huf


input_sequence = input("enter your sequence: ")
print("Input sequence: ", input_sequence)

coding = bw.coding_BWT(input_sequence)
print("BWT coding table:")
for i in range(len(coding.bwt_table)):
    print(coding.bwt_table[i])
print("\nThe BWT is: ", coding.bwt_concatcode)

count = huf.count_nucs(coding.bwt_concatcode)
print("The frequency for each character is: ", count)
compress = huf.huffman_encoding(count, coding.bwt_concatcode)
print("The binary code for the characters is: ", compress.letter_code)
print("\nBinary tree for the Huffman compression:")
for i in range(len(compress.nodes)):
    print(compress.nodes[i])
print("\nThe binary sequence is:\n", compress.huffman_bincode)
print("The compressed sequence is:\n", compress.ascii_code)

decompress = huf.huffman_decoding(compress.ascii_code, compress.added_zero, \
compress.letter_code)
print("The uncompressed binary sequenced is:\n", decompress.original_binseq)
print("The uncompressed BWT sequence is:\n", decompress.uncompressed_string)

decoding = bw.decoding_BWT(decompress.uncompressed_string)
print("\nThe decoding BWT table is:")
for i in range(len(decoding.decode)):
    print(decoding.decode[i])
print("\nThe original sequence is: " + decoding.decoded_seq)
