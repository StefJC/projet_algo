#!/usr/bin/env python3
# -*- coding: utf-8 -*-

import tkinter as tk
import tkinter.ttk as ttk
import tkinter.filedialog
import Burrows_Wheeler as bw
import Huffman as huf
from tkinter import messagebox


class Main(tk.Frame):
    '''Construction of the main menu of the application'''
    def __init__(self, master):
        tk.Frame.__init__(self, master, bg = '#55514a')
        self.master = master
        self.grid()
        self.title = tk.Label(self, text = \
        'Perform Burrows-Wheeler encoding/decoding and Huffman compression/decompression.'\
        +'\nPlease choose one of these options:', bg = '#55514a', wraplength = 720, \
        fg = "white", font = ("liberation", 14))
        #links to the different modules with buttons
        self.title.grid(row = 0, columnspan = 3, padx = 5, pady = 5)
        self.labelbttn1 = tk.Label(self, text = 'BWT encoding module', bg = \
        '#55514a', fg = "white", font = ("liberation", 11))
        self.labelbttn1.grid(row = 1, column = 0, padx = 5, pady = 5, sticky = 'W')
        self.code_bwt = tk.Button(self, text = 'Launch module', command = \
        self.bwtcoding_window)
        self.code_bwt.grid(row = 1, column = 1, padx = 5, pady = 5)
        self.labelbttn2 = tk.Label(self, text = 'BWT decoding module', bg = \
        '#55514a', fg = "white", font = ("liberation", 11))
        self.labelbttn2.grid(row = 2, column = 0, padx = 5, pady = 5, sticky = 'W')
        self.decode_bwt = tk.Button(self, text = 'Launch module', command = \
        self.bwtdecoding_window)
        self.decode_bwt.grid(row=2, column = 1, padx = 5, pady = 5)
        self.labelbttn3 = tk.Label(self, text='Huffman compression module',\
        bg = '#55514a', fg = "white", font=("liberation", 11))
        self.labelbttn3.grid(row=3, column = 0, padx = 5, pady=5, sticky = 'W')
        self.huff_compress = tk.Button(self, text = 'Launch module', command = \
        self.huffcompress_window)
        self.huff_compress.grid(row = 3, column = 1, padx = 5, pady = 5)
        self.labelbttn4 = tk.Label(self, text='Huffman decompression module',\
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelbttn4.grid(row = 4, column = 0, padx = 5, pady = 5, sticky = 'W')
        self.huff_decompress = tk.Button(self, text = 'Launch module', command = \
        self.huffdecompress_window)
        self.huff_decompress.grid(row = 4, column = 1, padx = 5, pady = 5)
        self.labelbttn5 = tk.Label(self, text = \
        'BWT coding + Huffman compression module', bg = '#55514a', fg = "white",\
        font = ("liberation", 11))
        self.labelbttn5.grid(row = 5, column = 0, padx = 5, pady = 5, sticky = 'W')
        self.bwthuff_compress = tk.Button(self, text = 'Launch module', command = \
        self.bwthuffcompress_window)
        self.bwthuff_compress.grid(row = 5, column = 1, padx = 5, pady = 5)
        self.labelbttn6 = tk.Label(self, text = \
        'Huffman decompression + BWT decoding module', bg = '#55514a', fg = \
        "white", font = ("liberation", 11))
        self.labelbttn6.grid(row = 6, column = 0, padx = 5, pady = 5, sticky = 'W')
        self.huffbwt_decompress = tk.Button(self, text = 'Launch module', \
        command = self.huffbwtdecompress_window)
        self.huffbwt_decompress.grid(row = 6, column = 1, padx = 5, pady = 5)

    def bwtcoding_window(self):
        '''Brings the user to the window dedicated to BWT encoding'''
        self.grid_forget()
        Bwtencoding(self.master)

    def bwtdecoding_window(self):
        '''Brings the user to the window dedicated to BWT decoding'''
        self.grid_forget()
        Bwtdecoding(self.master)

    def huffcompress_window(self):
        '''Brings the user to the window dedicated to Huffman compression'''
        self.grid_forget()
        Huffcompress(self.master)

    def huffdecompress_window(self):
        '''Brings the user to the window dedicated to Huffman decompression'''
        self.grid_forget()
        Huffdecompress(self.master)

    def bwthuffcompress_window(self):
        '''Brings the user to the window dedicated to the combo BWT coding/Huffman
        compression'''
        self.grid_forget()
        Bwthuffcompress(self.master)

    def huffbwtdecompress_window(self):
        '''Brings the user to the window dedicated to the combo Huffman
        decompression/BWT decoding'''
        self.grid_forget()
        Huffbwtdecompress(self.master)


class Bwtencoding(tk.Frame):
    '''Construction of the window dedicated to BWT encoding'''
    def __init__(self, master):
        tk.Frame.__init__(self, bg = '#55514a')
        self.master = master
        self.grid()
        #title
        self.title = tk.Label(self, text = 'Perform the BWT encoding of a string',\
        wraplength = 720, bg = '#55514a', fg = "white", font = ("liberation", 15))
        self.title.grid(row=0, columnspan = 3, padx = 5, pady=5)
        #open button
        self.labelopenbttn = tk.Label(self, text=\
        'Open a text file containing your string:', bg='#55514a', fg = "white", \
        font=("liberation", 11))
        self.labelopenbttn.grid(row = 1, column = 0, columnspan = 2, padx = 5,\
        pady = 5, sticky = 'W')
        self.openbttn = tk.Button(self, text="Open file", command = self.open_seq)
        self.openbttn.grid(row=1, column = 3, padx = 5, pady = 5)
        #construction of the Treeview used to show the BWT coding table
        self.bwtcode_table = ttk.Treeview(self)
        self.bwtcode_table["columns"] = ("one")
        self.bwtcode_table.column("#0", width = 0, minwidth = 0, stretch = tk.NO)
        self.bwtcode_table.column("one", width = 500, minwidth = 150, stretch = tk.NO)
        self.bwtcode_table.heading("#0",text = "")
        self.bwtcode_table.heading("one", text = "BWT table")
        self.bwtcode_table.grid(row = 2, padx = 5)
        ttk.Style().configure("Treeview", background = "#55514a", foreground = \
        "white", fieldbackground = "#55514a")
        #button bringing back to main menu
        self.labelbackbttn = tk.Label(self, text = \
        'Find more tools to manipulate strings in the main menu', bg='#55514a', \
        fg = "white", font = ("liberation", 11))
        self.labelbackbttn.grid(row = 9, column = 0, columnspan = 2, padx = 5, \
        pady=5, sticky = 'W')
        self.backbttn = tk.Button(self, text = "Back to main", command = \
        self.back_to_main)
        self.backbttn.grid(row = 9, column = 3, padx = 5, pady = 5)
        #counter for the construction of the BWT table line by line (index of the row)
        self.tablerow = 0

    def open_seq(self):
        '''function to open the file containing the string to code'''
        self.file = tk.filedialog.askopenfile(mode ='r', filetypes = \
        [('Text Files', '*.txt')])
        self.content = self.file.readlines()
        # test if the file contains a single string on one line
        if len(self.content) is not 1:
            messagebox.showerror("Error", \
            "Your file must contain a single string on one line")
            return
        #remove the \n present at the end of the string
        self.string = self.content[0].rstrip()
        #disable the "open" button to avoid successive openings
        self.openbttn = tk.Button(self, text="Open file", state=tk.DISABLED)
        self.openbttn.grid(row = 1, column = 3, padx = 5, pady = 5)
        #go to function constructing the BWT table
        self.construct_bwt_table()

    def construct_bwt_table(self):
        '''function including the different buttons used to construct the bwt table'''
        #run the function performing the BWT encoding
        self.code = bw.coding_BWT(self.string)
        #button for the construction line by line
        self.label1by1bttn = tk.Label(self, text = 'Construct table line by line',\
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.label1by1bttn.grid(row = 4, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", command = \
        self.show_1by1)
        self.bwt1by1bttn.grid(row = 4, column = 3, padx = 5, pady = 5)
        #button to show the complete BWT table
        self.labelallbttn = tk.Label(self, text = 'Show complete table', bg = \
        '#55514a', fg = "white", font = ("liberation", 11))
        self.labelallbttn.grid(row = 5, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.bwtallbttn = tk.Button(self, text = "show all", command = self.show_all)
        self.bwtallbttn.grid(row = 5, column = 3, padx = 5, pady = 5)
        #button to sort the BWT table and show the code
        self.labelallbttn = tk.Label(self, text = 'Sort table and show code', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelallbttn.grid(row = 6, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.resultbttn = tk.Button(self, text = "show code", command = self.show_code)
        self.resultbttn.grid(row = 6, column = 3, padx = 5, pady = 5)

    def show_1by1(self):
        '''Function allowing the construction of the BWT table line by line'''
        #insert the row in the Treeview according to its index
        bwt_1by1 = self.code.code_table[self.tablerow]
        self.bwtcode_table.insert("", "end", values = bwt_1by1)
        #increase the index number while in the length of the table
        if self.tablerow <= len(self.code.code_table):
            self.tablerow += 1
            #to avoid errors, disable buttons when construction of the table completed
            if self.tablerow == len(self.code.code_table):
                self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", state = \
                tk.DISABLED)
                self.bwt1by1bttn.grid(row = 4, column = 3, padx = 5, pady = 5)
                self.bwtallbttn = tk.Button(self, text = "show all", state = \
                tk.DISABLED)
                self.bwtallbttn.grid(row = 5, column = 3, padx = 5, pady = 5)

    def show_all(self):
        '''Function showing directly the complete BWT table'''
        #erase table partially constructed and disable buttons to avoid errors
        self.bwtcode_table.delete(*self.bwtcode_table.get_children())
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", state = tk.DISABLED)
        self.bwt1by1bttn.grid(row = 4, column = 3, padx = 5, pady = 5)
        self.bwtallbttn = tk.Button(self, text = "show all", state = tk.DISABLED)
        self.bwtallbttn.grid(row = 5, column = 3, padx = 5, pady = 5)
        #fill Treeview with the full BWT table
        for line in self.code.code_table:
            self.bwtcode_table.insert("", "end", values = line)

    def show_code(self):
        '''Function showing the sorted table and the BWT code'''
        #erase unsorted table and disable buttons to avoid errors
        self.bwtcode_table.delete(*self.bwtcode_table.get_children())
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", state = tk.DISABLED)
        self.bwt1by1bttn.grid(row = 4, column = 3, padx = 5, pady = 5)
        self.bwtallbttn = tk.Button(self, text = "show all", state = tk.DISABLED)
        self.bwtallbttn.grid(row = 5, column = 3, padx = 5, pady = 5)
        #fill Treeview with the sorted BWT table
        for line in self.code.bwt_table:
            self.bwtcode_table.insert("", "end", values = line)
        # show BWT code
        self.bwtcode = tk.Label(self, text = "Your code is: " + \
        self.code.bwt_concatcode, bg = '#55514a', fg = "white", font = \
        ("liberation", 11))
        self.bwtcode.grid(row = 3, column = 0, columnspan = 3, padx = 5, pady = 5)
        #inactivate result button
        self.resultbttn = tk.Button(self, text="show code", state = tk.DISABLED)
        self.resultbttn.grid(row = 6, column = 3, padx = 5, pady = 5)
        #show button to save the current job
        self.labelsavebttn = tk.Label(self, text = 'Save the BWT encoded sequence:', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelsavebttn.grid(row = 7, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.savebttn = tk.Button(self, text = "Save", command = self.save_bwtcode)
        #show button to erase the current job and start new
        self.savebttn.grid(row = 7, column = 3, padx = 5, pady = 5)
        self.labelerasebttn = tk.Label(self, text = 'Erase and start new job', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelerasebttn.grid(row = 8, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.erasebttn = tk.Button(self, text = "Erase", command = self.start_again)
        self.erasebttn.grid(row = 8, column = 3, padx = 5, pady = 5)

    def save_bwtcode(self):
        '''function allowing to save the current job'''
        save_file = tk.filedialog.asksaveasfile(mode = 'w', defaultextension = '.txt')
        if save_file is None:
            return
        save_file.write(self.code.bwt_concatcode)
        save_file.close()

    def back_to_main(self):
        '''Brings back the user to the main menu'''
        self.grid_forget()
        self.bwtcode_table.grid_forget()
        Main(self.master)

    def start_again(self):
        '''erase current job'''
        self.grid_forget()
        self.bwtcode_table.grid_forget()
        Bwtencoding(self.master)


class Bwtdecoding(tk.Frame):
    '''Construction of the window dedicated to BWT decoding'''
    def __init__(self, master):
        tk.Frame.__init__(self, bg='#55514a')
        self.master = master
        self.grid()
        self.title = tk.Label(self, text='Decode a BWT encoded string', \
        wraplength = 720, bg='#55514a', fg = "white", font=("liberation", 15))
        self.title.grid(row=0, columnspan = 3, padx = 5, pady = 5)
        #open button
        self.labelopenbttn = tk.Label(self, text = \
        'Open a text file containing your BWT code:', bg = '#55514a', \
        fg = "white", font = ("liberation", 11))
        self.labelopenbttn.grid(row = 1, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.openbttn = tk.Button(self, text = "Open file", command = self.open_seq)
        self.openbttn.grid(row = 1, column = 3, padx = 5, pady = 5)
        #construction of the Treeview used to show the BWT decoding table
        self.decode_table = ttk.Treeview(self)
        self.decode_table["columns"] = ("one")
        self.decode_table.column("#0", width = 0, minwidth = 0, stretch = tk.NO)
        self.decode_table.column("one", width = 500, minwidth = 150, stretch = tk.NO)
        self.decode_table.heading("#0",text = "")
        self.decode_table.heading("one", text = "BWT decoding table")
        self.decode_table.grid(row = 2)
        ttk.Style().configure("Treeview", background = "#55514a", foreground = \
        "white", fieldbackground = "#55514a")
        #button bringing back to main menu
        self.labelbackbttn = tk.Label(self, text =
        'Find more tools to manipulate strings in the main menu', bg = '#55514a',\
        fg = "white", font = ("liberation", 11))
        self.labelbackbttn.grid(row=9, column = 0, columnspan = 2, pady = 5, \
        sticky = 'W')
        self.backbttn = tk.Button(self, text = "Back to main", command = \
        self.back_to_main)
        self.backbttn.grid(row = 9, column = 3, padx = 5, pady = 5)
        #initialize list for the construction of the BWT decoding table
        self.bwt_code = []

    def open_seq(self):
        '''function to open the file containing the BWT encoded string to decode'''
        self.file = tk.filedialog.askopenfile(mode ='r', filetypes = \
        [('Text Files', '*.txt')])
        self.fullcontent = self.file.readlines()
        # test if the file contains a single string on one line
        if len(self.fullcontent) is not 1:
            messagebox.showerror("Error", \
            "Your file must contain a single string on one line")
            return
        #remove the \n present at the end of the string
        self.content = self.fullcontent[0].rstrip()
        #create a list with all the characters of the BWT code
        for char in range(len(self.content)):
            self.bwt_code.append(self.content[char])
        #create the list which will be used as the basis of the growing decode table
        self.decode = self.bwt_code
        #test if the string is a BWT encoded sequence
        try:
            bw.decoding_BWT(self.bwt_code)
        except UnboundLocalError:
            messagebox.showerror("Error", \
            "Your file must contain a BWT encoded string")
            self.bwt_code = []
            return
        #disable the "open" button to avoid successive openings
        self.openbttn = tk.Button(self, text="Open file", state=tk.DISABLED)
        self.openbttn.grid(row = 1, column = 3, padx = 5, pady = 5)
        #go to function for decoding
        self.construct_decode_table()

    def construct_decode_table(self):
        '''function including the different buttons used to construct
        the decoding table'''
        #button to construct the decoding table
        self.label1by1bttn = tk.Label(self, text = 'Construct table column by column',\
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.label1by1bttn.grid(row = 4, column = 0, columnspan = 2, pady = 5, \
        sticky = 'W')
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", command = \
        self.show_1by1)
        self.bwt1by1bttn.grid(row = 4, column = 3, padx = 5, pady = 5)
        #button to show the full table and the original string
        self.labelallbttn = tk.Label(self, text = \
        'Show complete table and original string', bg = '#55514a', fg = "white",\
        font = ("liberation", 11))
        self.labelallbttn.grid(row = 5, column = 0, columnspan = 2, pady = 5, \
        sticky = 'W')
        self.bwtallbttn = tk.Button(self, text = "show result", command = \
        self.show_result)
        self.bwtallbttn.grid(row = 5, column = 3, padx = 5, pady = 5)

    def show_1by1(self):
        '''Function allowing to construct decoding table column by column'''
        #clear current Treeview
        self.decode_table.delete(*self.decode_table.get_children())
        #grow decoding table while it is not completely filled,
        if len(self.decode[0]) < len(self.decode):
            #create columnb of the Treeview by inserting elements of list 1 by 1
            for row in self.decode:
                self.decode_table.insert("", "end", values = row)
            #when 1st column created, point "1 by 1" button to the sorting function
            self.bwt1by1bttn.grid_forget()
            self.bwt1by1bttn = tk.Button(self, text="show 1 by 1", command= \
            self.show_sorted)
            self.bwt1by1bttn.grid(row = 4, column = 3, padx = 5, pady = 5)
        #disable buttons constructing table when table completely filled
        elif len(self.decode[0]) == len(self.decode):
            self.bwt1by1bttn = tk.Button(self, text="show 1 by 1", state = tk.DISABLED)
            self.bwt1by1bttn.grid(row = 4, column = 3, padx = 5, pady = 5)
            self.bwtallbttn = tk.Button(self, text="show result", state = tk.DISABLED)
            self.bwtallbttn.grid(row = 5, column = 3, padx = 5, pady = 5)
            # and go to the function showing result
            self.show_result()

    def show_sorted(self):
        '''Function allowing sorting of the decoding table and to concatenate the
        BWT code on the sorted table'''
        #sort the decode table
        self.decode = sorted(self.decode)
        #clear current Treeview
        self.decode_table.delete(*self.decode_table.get_children())
        #insert sorted decode table in Treeview
        for row in self.decode:
            self.decode_table.insert("", "end", values = row)
        #extend sorted decode table with bwt code
        for i in range(len(self.decode)):
            self.decode.append(self.bwt_code[i] + self.decode[i])
        #remove the intermediate sorted code from the table
        del self.decode[0:len(self.bwt_code)]
        #go back to the function constructing the table to continue the extension
        self.construct_decode_table()

    def show_result(self):
        '''Function showing the full decoding table and the original sequence'''
        #clear current Treeview
        self.decode_table.delete(*self.decode_table.get_children())
        #run the decoding function from the Burrows_Wheeler library to get the
        #decoded sequence
        self.result = bw.decoding_BWT(self.bwt_code)
        #disable "show 1 by 1" button
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", state = tk.DISABLED)
        self.bwt1by1bttn.grid(row = 4, column = 3, padx = 5, pady = 5)
        #insert the sorted decoding table in the Treeview
        for line in self.result.decode:
            self.decode_table.insert("", "end", values = line)
        #show the decoded sequence
        self.bwtcode = tk.Label(self, text = "Your sequence is: " + \
        self.result.decoded_seq, bg = '#55514a', fg = "white", font = \
        ("liberation", 11), wraplength = 500)
        self.bwtcode.grid(row = 3, column = 0, columnspan = 3, padx = 5, pady = 5)
        #inactivate result button
        self.bwtallbttn = tk.Button(self, text="show result", state = tk.DISABLED)
        self.bwtallbttn.grid(row = 5, column = 3, padx = 5, pady = 5)
        #show button to save the current job
        self.labelsavebttn = tk.Label(self, text = 'Save the BWT decoded sequence:', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelsavebttn.grid(row = 7, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.savebttn = tk.Button(self, text = "Save", command = self.save_decode)
        self.savebttn.grid(row = 7, column = 3, padx = 5, pady = 5)
        #show button to erase the current job and start a new one
        self.labelerasebttn = tk.Label(self, text = 'Erase and start new job', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelerasebttn.grid(row = 8, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.erasebttn = tk.Button(self, text = "Erase", command = self.start_again)
        self.erasebttn.grid(row = 8, column = 3, padx = 5, pady = 5)

    def save_decode(self):
        '''function allowing to save the current job'''
        save_file = tk.filedialog.asksaveasfile(mode = 'w', defaultextension = '.txt')
        if save_file is None:
            return
        save_file.write(self.result.decoded_seq)
        save_file.close()

    def back_to_main(self):
        '''Brings back the user to the main menu'''
        self.grid_forget()
        self.decode_table.grid_forget()
        Main(self.master)

    def start_again(self):
        '''erase current job'''
        self.grid_forget()
        self.decode_table.grid_forget()
        Bwtdecoding(self.master)


class Huffcompress(tk.Frame):
    '''Construction of the window dedicated to Huffman compression'''
    def __init__(self, master):
        tk.Frame.__init__(self, bg = '#55514a')
        self.master = master
        self.grid()
        #title
        self.title = tk.Label(self, text=\
        'Perform the Huffman compression of a sequence or of a BWT encoded sequence',\
        wraplength = 720, bg = '#55514a', fg = "white", font = ("liberation", 15))
        self.title.grid(row = 0, columnspan = 3, padx = 5, pady = 5)
        #open button
        self.labelopenbttn = tk.Label(self, text = \
        'Open a text file containing your sequence or your BWT code:', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelopenbttn.grid(row = 1, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.openbttn = tk.Button(self, text="Open file", command=self.open_seq)
        self.openbttn.grid(row=1, column = 3, padx = 5, pady = 5)
        #construction of the Treeview used to show the binary tree
        self.node_table = ttk.Treeview(self)
        self.node_table["columns"] = ("one", "two", "three", "four")
        self.node_table.column("#0", width = 0, minwidth = 0, stretch = tk.NO)
        self.node_table.column("one", width = 150, minwidth = 150, stretch = tk.NO)
        self.node_table.column("two", width = 150, minwidth = 150, stretch = tk.NO)
        self.node_table.column("three", width = 150, minwidth = 150, stretch = tk.NO)
        self.node_table.column("four", width = 150, minwidth = 150, stretch = tk.NO)

        self.node_table.heading("#0",text = "")
        self.node_table.heading("one", text = "Nodes")
        self.node_table.heading("two", text = "child1")
        self.node_table.heading("three", text = "child2")
        self.node_table.heading("four", text = "branch value")
        self.node_table.grid(row = 2)
        ttk.Style().configure("Treeview", background = "#55514a", \
        foreground = "white", fieldbackground = "#55514a")
        #button bringing back to main menu
        self.labelbackbttn = tk.Label(self, text = \
        'Find more tools to manipulate strings in the main menu', bg = '#55514a',\
        fg = "white", font = ("liberation", 11))
        self.labelbackbttn.grid(row = 9, column = 0, columnspan = 2, pady = 5, \
        sticky = 'W')
        self.backbttn = tk.Button(self, text = "Back to main", command = \
        self.back_to_main)
        self.backbttn.grid(row = 9, column = 3, padx = 5, pady = 5)

    def open_seq(self):
        '''function to open the file containing the string to compress'''
        self.file = tk.filedialog.askopenfile(mode ='r', filetypes = \
        [('Text Files', '*.txt')])
        self.fullcontent = self.file.readlines()
        # test if the file contains a single string on one line
        if len(self.fullcontent) is not 1:
            messagebox.showerror("Error", \
            "Your file must contain a single string on one line")
            return
        #remove the \n present at the end of the string
        self.content = self.fullcontent[0].rstrip()
        #disable the "open" button to avoid successive openings
        self.openbttn = tk.Button(self, text="Open file", state=tk.DISABLED)
        self.openbttn.grid(row = 1, column = 3, padx = 5, pady = 5)
        #go to the function dedicated to compression
        self.huffman_compress()

    def huffman_compress(self):
        '''function allowing the compression of the sequence'''
        #calculate frequency of each letter using the dedicated function in library
        count = huf.count_nucs(self.content)
        #run the encoding function from the library
        self.compress = huf.huffman_encoding(count, self.content)
        col_select=[]
        #get the elements from the binary tree to insert them in the Treeview
        for i in self.compress.nodes:
            parser = str(i).split()
            #root node does not have a binary value. Find it and insert "None" instead
            if len(parser) is not 9:
                parser.insert(8, "None")
            col_select = [parser[2], parser[4], parser[6], parser[8]]
            self.node_table.insert("", "end", values = col_select)
        #show binary sequence
        self.binseq = tk.Label(self, text = "The binary code is: " + \
        self.compress.huffman_bincode, wraplength = 500, bg = '#55514a', fg = \
        "white", font = ("liberation", 11))
        self.binseq.grid(row = 3, column = 0, columnspan = 3, padx = 5, pady = 5)
        #show compressed ascii sequence
        self.asciiseq = tk.Label(self, text = "The compressed sequence is: " + \
        self.compress.ascii_code, wraplength = 500, bg = '#55514a', fg = "white",\
        font = ("liberation", 11))
        self.asciiseq.grid(row = 4, column = 0, columnspan = 3, padx = 5, pady = 5)
        #show button to save the current job
        self.labelsavebttn = tk.Label(self, text = 'Save the compressed sequence:',\
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelsavebttn.grid(row = 7, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.savebttn = tk.Button(self, text = "Save", command = self.save_compress)
        self.savebttn.grid(row = 7, column = 3, padx = 5, pady = 5)
        #show button to erase the current job and start a new one
        self.labelerasebttn = tk.Label(self, text = 'Erase and start new job', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelerasebttn.grid(row = 8, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.erasebttn = tk.Button(self, text = "Erase", command = self.start_again)
        self.erasebttn.grid(row = 8, column = 3, padx = 5, pady = 5)

    def save_compress(self):
        '''function allowing to save the current job'''
        #to do the decompression, ascii sequence, number of added zeros in binary
        #sequence and the binary code for each letter are needed
        compression_file = self.compress.ascii_code + "\n" + \
        str(self.compress.added_zero) + "\n" + str(self.compress.letter_code)
        save_file = tk.filedialog.asksaveasfile(mode = 'w', defaultextension='.txt')
        if save_file is None:
            return
        save_file.write(compression_file)
        save_file.close()

    def back_to_main(self):
        '''Brings back the user to the main menu'''
        self.grid_forget()
        self.node_table.grid_forget()
        Main(self.master)

    def start_again(self):
        '''erase current job'''
        self.grid_forget()
        self.node_table.grid_forget()
        Huffcompress(self.master)


class Huffdecompress(tk.Frame):
    '''Construction of the window dedicated to Huffman decompression'''
    def __init__(self, master):
        tk.Frame.__init__(self, bg = '#55514a')
        self.master = master
        self.grid()
        #title
        self.title = tk.Label(self, text = \
        'Perform the Huffman decompression of a compressed string', \
        wraplength = 720, bg = '#55514a', fg = "white", font = ("liberation", 15))
        self.title.grid(row = 0, columnspan = 3, padx = 5, pady = 5)
        #open button
        self.labelopenbttn = tk.Label(self, text = \
        'Open a text file containing your sequence or your BWT code:', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelopenbttn.grid(row = 1, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.openbttn = tk.Button(self, text = "Open file", command = self.open_seq)
        self.openbttn.grid(row = 1, column = 3, padx = 5, pady = 5)
        #button bringing back to main menu
        self.labelbackbttn = tk.Label(self, text = \
        'Find more tools to manipulate strings in the main menu', bg = '#55514a',\
        fg = "white", font = ("liberation", 11))
        self.labelbackbttn.grid(row = 9, column = 0, columnspan = 2, pady = 5, \
        sticky = 'W')
        self.backbttn = tk.Button(self, text = "Back to main", command = \
        self.back_to_main)
        self.backbttn.grid(row = 9, column = 3, padx = 5, pady = 5)

    def open_seq(self):
        '''function to open the compressed sequence'''
        self.file = tk.filedialog.askopenfile(mode ='r', filetypes = \
        [('Text Files', '*.txt')])
        self.content = self.file.readlines()
        #test whether the file was generated with the Huffman compression module
        try:
            self.content[1] and self.content[2] #the file must contain 3 lines
        except IndexError:
            messagebox.showerror("Error", \
            "Use a file compressed with this application")
            return
        try:
            eval(self.content[2]) #the 3rd line must correspond to a dictionary
        except SyntaxError:
            messagebox.showerror("Error", \
            "Use a file compressed with this application")
            return
        #get the compressed ascii sequence and remove the \n at the end of the string
        self.compressed = self.content[0]
        self.compressed = self.compressed[:-1]
        #get the number of added zeros in binary code and change as integer
        self.zerocount = int(self.content[1])
        #get the string corresponding to the dictionnary containing the binary
        #code for each letter and run it as a python expression to build dictionary
        self.dico_code = eval(self.content[2])
        #disable the "open" button to avoid successive openings
        self.openbttn = tk.Button(self, text="Open file", state=tk.DISABLED)
        self.openbttn.grid(row = 1, column = 3, padx = 5, pady = 5)
        #go to the function dedicated to decompression
        self.huffman_decompress()

    def huffman_decompress(self):
        '''function allowing the decompression of the string'''
        #run the decoding function from the dedicated library
        self.decompress = huf.huffman_decoding(self.compressed, self.zerocount, \
        self.dico_code)
        #show the binary sequence
        self.binseq = tk.Label(self, text = "The uncompressed binary code is: " \
        + self.decompress.original_binseq, wraplength = 500, bg = '#55514a', \
        fg = "white", font=("liberation", 11))
        self.binseq.grid(row = 3, column = 0, columnspan = 3, padx = 5, pady = 5)
        #show the decompressed original sequence
        self.yourseq = tk.Label(self, text = "The original sequence is: " + \
        self.decompress.uncompressed_string, wraplength = 500, bg = '#55514a', \
        fg = "white", font=("liberation", 11))
        self.yourseq.grid(row = 4, column = 0, columnspan = 3, padx = 5, pady = 5)
        #show buttons needed to save or erase the current job
        self.labelsavebttn = tk.Label(self, text = 'Save the original sequence:',\
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelsavebttn.grid(row = 7, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.savebttn = tk.Button(self, text = "Save", command = self.save_decompress)
        self.savebttn.grid(row = 7, column = 3, padx = 5, pady = 5)
        self.labelerasebttn = tk.Label(self, text = 'Erase and start new job', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelerasebttn.grid(row = 8, column = 0, columnspan = 2, padx = 5, \
        pady=5, sticky = 'W')
        self.erasebttn = tk.Button(self, text = "Erase", command = self.start_again)
        self.erasebttn.grid(row = 8, column = 3, padx = 5, pady = 5)

    def save_decompress(self):
        '''function to save the current job'''
        decomp_sequ = self.decompress.uncompressed_string
        save_file = tk.filedialog.asksaveasfile(mode = 'w', defaultextension='.txt')
        if save_file is None:
            return
        save_file.write(decomp_sequ)
        save_file.close()

    def back_to_main(self):
        '''Brings back the user to the main menu'''
        self.grid_forget()
        Main(self.master)

    def start_again(self):
        '''erase current job'''
        self.grid_forget()
        Huffdecompress(self.master)


class Bwthuffcompress(tk.Frame):
    '''Construction of the window dedicated to BWT coding + Huffman compression'''
    def __init__(self, master):
        tk.Frame.__init__(self, bg = '#55514a')
        self.master = master
        self.grid()
        #title
        self.title = tk.Label(self, text = \
        'Perform the BWT encoding and Huffman compression of a string',\
        wraplength = 720, bg = '#55514a', fg = "white", font = ("liberation", 15))
        self.title.grid(row=0, columnspan = 3, padx = 5)
        #open button
        self.labelopenbttn = tk.Label(self, text = \
        'Open a text file containing your string:', bg='#55514a', fg = "white", \
        font=("liberation", 11))
        self.labelopenbttn.grid(row = 2, column = 0, columnspan = 2, padx = 5,\
        pady = 2, sticky = 'W')
        self.openbttn = tk.Button(self, text="Open file", command = self.open_seq)
        self.openbttn.grid(row=2, column = 1, padx = 5, pady = 2)
        #construction of the Treeview used to show the BWT coding table
        self.bwtcode_table = ttk.Treeview(self)
        self.bwtcode_table["columns"] = ("one")
        self.bwtcode_table.column("#0", width = 0, minwidth = 0, stretch = tk.NO)
        self.bwtcode_table.column("one", width = 500, minwidth = 150, stretch = tk.NO)
        self.bwtcode_table.heading("#0",text = "")
        self.bwtcode_table.heading("one", text = "BWT table")
        self.bwtcode_table.grid(row = 3, column = 0, columnspan = 2, padx = 5, pady = 2)
        ttk.Style().configure("Treeview", background = "#55514a", foreground = \
        "white", fieldbackground = "#55514a", rowheight = 15)
        #counter for the construction of the BWT table line by line (index of the row)
        self.tablerow = 0
        #construction of the Treeview used to show the binary tree for compression
        self.node_table = ttk.Treeview(self)
        self.node_table["columns"]=("one", "two", "three", "four")
        self.node_table.column("#0", width=0, minwidth=0, stretch=tk.NO)
        self.node_table.column("one", width=150, minwidth=150, stretch=tk.NO)
        self.node_table.column("two", width=150, minwidth=150, stretch=tk.NO)
        self.node_table.column("three", width=150, minwidth=150, stretch=tk.NO)
        self.node_table.column("four", width=150, minwidth=150, stretch=tk.NO)
        self.node_table.heading("#0",text="")
        self.node_table.heading("one", text="Nodes")
        self.node_table.heading("two", text="child1")
        self.node_table.heading("three", text="child2")
        self.node_table.heading("four", text="branch value")
        self.node_table.grid(row = 5, column = 0, columnspan = 2, padx = 5, pady = 2)
        ttk.Style().configure("Treeview", background = "#55514a", foreground = \
        "white", fieldbackground = "#55514a", rowheight = 15)
        #button bringing back to main menu
        self.labelbackbttn = tk.Label(self, text = \
        'Find more tools to manipulate strings in the main menu', bg='#55514a', \
        fg = "white", font = ("liberation", 11), wraplength = 350)
        self.labelbackbttn.grid(row = 13, column = 0, columnspan = 2, pady = 2, \
        sticky = 'W')
        self.backbttn = tk.Button(self, text = "Back to main", command = \
        self.back_to_main)
        self.backbttn.grid(row = 13, column = 1, padx = 5, pady = 2)

    def open_seq(self):
        '''function to open the file containing the string to code'''
        self.file = tk.filedialog.askopenfile(mode ='r', filetypes = \
        [('Text Files', '*.txt')])
        self.content = self.file.readlines()
        # test if the file contains a single string on one line
        if len(self.content) is not 1:
            messagebox.showerror("Error", \
            "Your file must contain a single string on one line")
            return
        #remove the \n present at the end of the string
        self.string = self.content[0].rstrip()
        #disable the "open" button to avoid successive openings
        self.openbttn = tk.Button(self, text="Open file", state=tk.DISABLED)
        self.openbttn.grid(row = 2, column = 1, padx = 5, pady = 2)
        #go to function to construct BWT coding table
        self.construct_bwt_table()

    def construct_bwt_table(self):
        '''function including the different buttons used to construct the bwt table'''
        #run the function performing the BWT encoding
        self.code = bw.coding_BWT(self.string)
        #button to construct table line by line
        self.label1by1bttn = tk.Label(self, text = 'Construct table line by line',\
        bg = '#55514a', fg = "white", font = ("liberation", 11), wraplength = 150)
        self.label1by1bttn.grid(row = 8, column = 0, pady = 2)
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", command = \
        self.show_1by1)
        self.bwt1by1bttn.grid(row = 9, column = 0, padx = 5, pady = 2)
        #button to show complete table
        self.labelallbttn = tk.Label(self, text = 'Show complete table', bg = \
        '#55514a', fg = "white", font = ("liberation", 11), wraplength = 150)
        self.labelallbttn.grid(row = 8, column = 1, pady = 2)
        self.bwtallbttn = tk.Button(self, text = "show all", command = self.show_all)
        self.bwtallbttn.grid(row = 9, column = 1, padx = 5, pady = 2)
        #button to sort the table and show the BWT code
        self.labelallbttn = tk.Label(self, text = 'Sort table and show code', \
        bg = '#55514a', fg = "white", font = ("liberation", 11), wraplength = 150)
        self.labelallbttn.grid(row = 8, column = 2, pady = 2,)
        self.resultbttn = tk.Button(self, text = "show code", command = \
        self.show_code)
        self.resultbttn.grid(row = 9, column = 2, padx = 5, pady = 2)

    def show_1by1(self):
        '''Function allowing the construction of the BWT table line by line'''
        #insert the row in the Treeview according to its index
        bwt_1by1 = self.code.code_table[self.tablerow]
        self.bwtcode_table.insert("", "end", values = bwt_1by1)
        #increase the index number while in the length of the table
        if self.tablerow <= len(self.code.code_table):
            self.tablerow += 1
            #disable buttons when construction of the table completed to avoid errors
            if self.tablerow == len(self.code.code_table):
                self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", state = \
                tk.DISABLED)
                self.bwt1by1bttn.grid(row = 9, column = 0, padx = 5, pady = 2)
                self.bwtallbttn = tk.Button(self, text = "show all", state = \
                tk.DISABLED)
                self.bwtallbttn.grid(row = 9, column = 1, padx = 5, pady = 2)

    def show_all(self):
        '''Function showing directly the complete BWT table'''
        #erase table partially constructed and disable "show 1 by 1" button
        self.bwtcode_table.delete(*self.bwtcode_table.get_children())
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", state = tk.DISABLED)
        self.bwt1by1bttn.grid(row = 9, column = 0, padx = 5, pady = 2)
        #fill Treeview with the full BWT table
        for line in self.code.code_table:
            self.bwtcode_table.insert("", "end", values = line)

    def show_code(self):
        '''Function showing the sorted table and the BWT code'''
        #erase unsorted table and disable other buttons to avoid errors
        self.bwtcode_table.delete(*self.bwtcode_table.get_children())
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", state = tk.DISABLED)
        self.bwt1by1bttn.grid(row = 9, column = 0, padx = 5, pady = 2)
        self.bwtallbttn = tk.Button(self, text = "show all", state = tk.DISABLED)
        self.bwtallbttn.grid(row = 9, column = 1, padx = 5, pady = 2)
        #fill Treeview with the sorted BWT table
        for line in self.code.bwt_table:
            self.bwtcode_table.insert("", "end", values = line)
        # show BWT code
        self.bwtcode = tk.Label(self, text = "Your BWT code is: " + \
        self.code.bwt_concatcode, bg = '#55514a', fg = "white", font = \
        ("liberation", 11))
        self.bwtcode.grid(row = 4, column = 0, columnspan = 3, padx = 5, pady = 2)
        #inactivate result button
        self.resultbttn = tk.Button(self, text="show code", state = tk.DISABLED)
        self.resultbttn.grid(row = 9, column = 2, padx = 5, pady = 2)
        #go to function dedicated to compression
        self.huffman_compress()

    def huffman_compress(self):
        '''function allowing the compression of the sequence'''
        #calculate frequency of each letter using the dedicated function in library
        count = huf.count_nucs(self.code.bwt_concatcode)
        #run the encoding function from the library
        self.compress = huf.huffman_encoding(count, self.code.bwt_concatcode)
        col_select=[]
        #get the elements from the binary tree to insert them in the Treeview
        for i in self.compress.nodes:
            parser = str(i).split()
            #root node does not have a binary value. Find it and insert "None" instead
            if len(parser) is not 9:
                parser.insert(8, "None")
            col_select = [parser[2], parser[4], parser[6], parser[8]]
            self.node_table.insert("", "end", values = col_select)
        #show binary sequence
        self.binseq = tk.Label(self, text = "Binary code: " + \
        self.compress.huffman_bincode, wraplength = 720, bg = '#55514a', fg = \
        "white", font = ("liberation", 11))
        self.binseq.grid(row = 6, column = 0, columnspan = 3, padx = 5, pady = 2)
        #show compressed ascii sequence
        self.asciiseq = tk.Label(self, text = "Compressed sequence: " + \
        self.compress.ascii_code, wraplength = 720, bg = '#55514a', fg = "white",\
        font = ("liberation", 11))
        self.asciiseq.grid(row = 7, column = 0, columnspan = 3, padx = 5, pady = 2)
        #show button to save the current job
        self.labelsavebttn = tk.Label(self, text = 'Save the compressed sequence:',\
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelsavebttn.grid(row = 10, column = 0, padx = 5, \
        pady = 2, sticky = 'W')
        self.savebttn = tk.Button(self, text = "Save", command = self.save_compress)
        self.savebttn.grid(row = 10, column = 1, padx = 5, pady = 2)
        #show button to erase the current job and start a new one
        self.labelerasebttn = tk.Label(self, text = 'Erase and start new job', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelerasebttn.grid(row = 11, column = 0, padx = 5, \
        pady = 2, sticky = 'W')
        self.erasebttn = tk.Button(self, text = "Erase", command = self.start_again)
        self.erasebttn.grid(row = 11, column = 1, padx = 5, pady = 2)

    def save_compress(self):
        '''function allowing to save the current job'''
        #to do the decompression, ascii sequence, number of added zeros in binary
        #sequence and the binary code for each letter are needed
        compression_file = self.compress.ascii_code + "\n" + \
        str(self.compress.added_zero) + "\n" + str(self.compress.letter_code)
        save_file = tk.filedialog.asksaveasfile(mode = 'w', defaultextension='.txt')
        if save_file is None:
            return
        save_file.write(compression_file)
        save_file.close()

    def back_to_main(self):
        '''Brings back the user to the main menu'''
        self.grid_forget()
        self.node_table.grid_forget()
        self.bwtcode_table.grid_forget()
        Main(self.master)

    def start_again(self):
        '''erase current job'''
        self.grid_forget()
        self.node_table.grid_forget()
        self.bwtcode_table.grid_forget()
        Bwthuffcompress(self.master)


class Huffbwtdecompress(tk.Frame):
    '''Construction of the window dedicated to Huffman decompression + BWT decoding'''
    def __init__(self, master):
        tk.Frame.__init__(self, bg = '#55514a')
        self.master = master
        self.grid()
        #title
        self.title = tk.Label(self, text = \
        'Perform the Huffman decompression and BWT decoding of\na BWT encoded/Huffman compressed string', \
        wraplength = 720, bg = '#55514a', fg = "white", font = ("liberation", 15))
        self.title.grid(row = 0, columnspan = 3, padx = 5, pady = 5)
        #open button
        self.labelopenbttn = tk.Label(self, text = \
        'Open a text file containing your BWT encoded/Huffman compressed string:',\
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelopenbttn.grid(row = 1, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.openbttn = tk.Button(self, text = "Open file", command = self.open_seq)
        self.openbttn.grid(row = 1, column = 3, padx = 5, pady = 5)
        #construction of the Treeview used to show the BWT decoding table
        self.decode_table = ttk.Treeview(self)
        self.decode_table["columns"] = ("one")
        self.decode_table.column("#0", width = 0, minwidth = 0, stretch = tk.NO)
        self.decode_table.column("one", width = 500, minwidth = 150, stretch = tk.NO)
        self.decode_table.heading("#0",text = "")
        self.decode_table.heading("one", text = "BWT decoding table")
        self.decode_table.grid(row = 2)
        ttk.Style().configure("Treeview", background = "#55514a", foreground = \
        "white", fieldbackground = "#55514a")
        #button bringing back to main menu
        self.labelbackbttn = tk.Label(self, text = \
        'Find more tools to manipulate strings in the main menu', bg = '#55514a',\
        fg = "white", font = ("liberation", 11))
        self.labelbackbttn.grid(row = 10, column = 0, columnspan = 2, pady = 5, sticky = 'W')
        self.backbttn = tk.Button(self, text="Back to main", command = self.back_to_main)
        self.backbttn.grid(row = 10, column = 3, padx = 5, pady = 5)
        #initialize list for the construction of the BWT decoding table
        self.bwt_code = []

    def open_seq(self):
        '''function to open the compressed sequence'''
        self.file = tk.filedialog.askopenfile(mode ='r', filetypes = [('Text Files', '*.txt')])
        self.content = self.file.readlines()
        #test whether the file was generated with the Huffman compression module
        try:
            self.content[1] and self.content[2] #the file must contain 3 lines
        except IndexError:
            messagebox.showerror("Error","Use a file compressed with this application")
            return
        try:
            eval(self.content[2]) #the 3rd line must correspond to a dictionary
        except SyntaxError:
            messagebox.showerror("Error","Use a file compressed with this application")
            return
        #get the compressed ascii sequence and remove the \n at the end of the string
        self.compressed = self.content[0]
        self.compressed = self.compressed[:-1]
        #get the number of added zeros in binary code and change as integer
        self.zerocount = int(self.content[1])
        #get the string corresponding to the dictionnary containing the binary
        #code for each letter and run it as a python expression to build dictionary
        self.dico_code = eval(self.content[2])
        #disable the "open" button to avoid successive openings
        self.openbttn = tk.Button(self, text="Open file", state=tk.DISABLED)
        self.openbttn.grid(row = 1, column = 3, padx = 5, pady = 5)
        #go to function dedicated to decompression
        self.huffman_decompress()

    def huffman_decompress(self):
        '''function allowing the decompression of the string'''
        #run the decoding function from the dedicated library
        self.decompress = huf.huffman_decoding(self.compressed, self.zerocount, \
        self.dico_code)
        #show the binary sequence
        self.binseq = tk.Label(self, text = "The uncompressed binary code is: " \
        + self.decompress.original_binseq, wraplength = 500, bg = '#55514a', \
        fg = "white", font=("liberation", 11))
        self.binseq.grid(row = 3, column = 0, columnspan = 3, padx = 5, pady = 5)
        #show the decompressed sequence
        self.yourseq = tk.Label(self, text = "The BWT encoded sequence is: " + \
        self.decompress.uncompressed_string, wraplength = 500, bg = '#55514a', \
        fg = "white", font=("liberation", 11))
        self.yourseq.grid(row = 4, column = 0, columnspan = 3, padx = 5, pady = 5)
        #create a list with all the characters of the BWT code
        for char in range(len(self.decompress.uncompressed_string)):
            self.bwt_code.append(self.decompress.uncompressed_string[char])
        #create the list which will be used as the basis of the growing decode table
        self.decode = self.bwt_code
        #go to the function constructing the BWT decoding table
        self.construct_decode_table()

    def construct_decode_table(self):
        '''function including the different buttons used to construct
        the decoding table'''
        #button to show the table column by column
        self.label1by1bttn = tk.Label(self, text = \
        'Construct BWT decoding table column by column', bg = '#55514a', fg = \
        "white", font = ("liberation", 11))
        self.label1by1bttn.grid(row = 6, column = 0, columnspan = 2, pady = 5, \
        sticky = 'W')
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", command = \
        self.show_1by1)
        self.bwt1by1bttn.grid(row = 6, column = 3, padx = 5, pady = 5)
        #button to show the table and the decoded string
        self.labelallbttn = tk.Label(self, text = \
        'Show complete table and original string', bg = '#55514a', fg = "white",\
        font = ("liberation", 11))
        self.labelallbttn.grid(row = 7, column = 0, columnspan = 2, pady = 5, \
        sticky = 'W')
        self.bwtallbttn = tk.Button(self, text = "show result", command = \
        self.show_result)
        self.bwtallbttn.grid(row = 7, column = 3, padx = 5, pady = 5)

    def show_1by1(self):
        '''Function allowing to construct decoding table column by column'''
        #clear current Treeview
        self.decode_table.delete(*self.decode_table.get_children())
        #grow decoding table while it is not completely filled,
        if len(self.decode[0]) < len(self.decode):
            #create columnb of the Treeview by inserting elements of list 1 by 1
            for row in self.decode:
                self.decode_table.insert("", "end", values = row)
            #when 1st column created, point "1 by 1" button to the sorting function
            self.bwt1by1bttn.grid_forget()
            self.bwt1by1bttn = tk.Button(self, text="show 1 by 1", command= \
            self.show_sorted)
            self.bwt1by1bttn.grid(row = 6, column = 3, padx = 5, pady = 5)
        #disable buttons constructing table when table completely filled
        elif len(self.decode[0]) == len(self.decode):
            self.bwt1by1bttn = tk.Button(self, text="show 1 by 1", state = \
            tk.DISABLED)
            self.bwt1by1bttn.grid(row = 6, column = 3, padx = 5, pady = 5)
            self.bwtallbttn = tk.Button(self, text="show result", state = \
            tk.DISABLED)
            self.bwtallbttn.grid(row = 7, column = 3, padx = 5, pady = 5)
            # and go to the function showing result
            self.show_result()

    def show_sorted(self):
        '''Function allowing sorting of the decoding table and extension with
        BWT code'''
        #sort the decode table
        self.decode = sorted(self.decode)
        #clear current Treeview
        self.decode_table.delete(*self.decode_table.get_children())
        #insert sorted decode table in Treeview
        for row in self.decode:
            self.decode_table.insert("", "end", values = row)
        #extend sorted decode table with bwt code
        for i in range(len(self.decode)):
            self.decode.append(self.bwt_code[i] + self.decode[i])
        #remove the intermediate sorted code from the table
        del self.decode[0:len(self.bwt_code)]
        #go back to the function constructing the table to continue the extension
        self.construct_decode_table()

    def show_result(self):
        self.decode_table.delete(*self.decode_table.get_children())
        #test if the string is a BWT encoded sequence, if not, must start again
        try:
            bw.decoding_BWT(self.bwt_code)
        except UnboundLocalError:
            messagebox.showerror("Error", \
            "Your compressed string was not BWT encoded.\nPlease erase job and open a new file")
            self.labelerasebttn = tk.Label(self, text = 'Erase and start new job', \
            bg = '#55514a', fg = "white", font = ("liberation", 11))
            self.labelerasebttn.grid(row = 9, column = 0, columnspan = 2, \
            padx = 5, pady = 5, sticky = 'W')
            self.erasebttn = tk.Button(self, text = "Erase", command = \
            self.start_again)
            self.erasebttn.grid(row = 9, column = 3, padx = 5, pady = 5)
            self.bwt1by1bttn = tk.Button(self, text="show 1 by 1", state = \
            tk.DISABLED)
            self.bwt1by1bttn.grid(row = 6, column = 3, padx = 5, pady = 5)
            self.bwtallbttn = tk.Button(self, text="show result", state = \
            tk.DISABLED)
            self.bwtallbttn.grid(row = 7, column = 3, padx = 5, pady = 5)
            return
        #run the decoding function from the Burrows_Wheeler library
        self.result = bw.decoding_BWT(self.bwt_code)
        #disable "show 1 by 1" button
        self.bwt1by1bttn = tk.Button(self, text = "show 1 by 1", state = tk.DISABLED)
        self.bwt1by1bttn.grid(row = 6, column = 3, padx = 5, pady = 5)
        #insert the sorted decoding table in the Treeview
        for line in self.result.decode:
            self.decode_table.insert("", "end", values = line)
        #show the decoded sequence
        self.bwtcode = tk.Label(self, text = "Your sequence is: " + \
        self.result.decoded_seq, bg = '#55514a', fg = "white", font = \
        ("liberation", 11), wraplength = 500)
        self.bwtcode.grid(row = 5, column = 0, columnspan = 3, padx = 5, pady = 5)
        #inactivate result button
        self.bwtallbttn = tk.Button(self, text="show result", state = tk.DISABLED)
        self.bwtallbttn.grid(row = 7, column = 3, padx = 5, pady = 5)
        #show button to save the current job
        self.labelsavebttn = tk.Label(self, text = 'Save the original sequence:', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelsavebttn.grid(row = 8, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.savebttn = tk.Button(self, text = "Save", command = self.save_decode)
        self.savebttn.grid(row = 8, column = 3, padx = 5, pady = 5)
        #show button to erase the current job and start a new one
        self.labelerasebttn = tk.Label(self, text = 'Erase and start new job', \
        bg = '#55514a', fg = "white", font = ("liberation", 11))
        self.labelerasebttn.grid(row = 9, column = 0, columnspan = 2, padx = 5, \
        pady = 5, sticky = 'W')
        self.erasebttn = tk.Button(self, text = "Erase", command = self.start_again)
        self.erasebttn.grid(row = 9, column = 3, padx = 5, pady = 5)

    def save_decode(self):
        '''function allowing to save the current job'''
        save_file = tk.filedialog.asksaveasfile(mode = 'w', defaultextension = '.txt')
        if save_file is None:
            return
        save_file.write(self.result.decoded_seq)
        save_file.close()

    def back_to_main(self):
        '''Brings back the user to the main menu'''
        self.grid_forget()
        self.decode_table.grid_forget()
        Main(self.master)

    def start_again(self):
        '''erase current job'''
        self.grid_forget()
        self.decode_table.grid_forget()
        Huffbwtdecompress(self.master)


if __name__ == "__main__":
    root = tk.Tk()
    root.title("Under pressure")
    root.geometry("780x680")
    root.minsize(480, 360)
    root.config(background='#55514a')
    app = Main(root)
    root.mainloop()
