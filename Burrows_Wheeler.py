#!/usr/bin/env python3
# -*- coding: utf-8 -*-

''' This script applies the Burrows-Wheeler transform to an input sequence using
the coding_BWT function and decodes it to give back the original sequence using
the decoding_BWT function'''


class BwtCodingResult:
    '''This class retrieves from the coding_BWT function, the BWT coding table and
    the BWT code as a list and as a string'''
    def __init__(self, code_table, bwt_table, bwt_code, bwt_concatcode):
        self.code_table = code_table
        self.bwt_table = bwt_table
        self.bwt_code = bwt_code
        self.bwt_concatcode = bwt_concatcode

def coding_BWT(sequence):
    """This function applies the Burrows-Wheeler transform to an input string
    and returns the BWT sorted table and the BWT code"""
    #add a symbol at the end of the sequence (required for decoding)
    sequence = sequence + '$'
    #generate the BWT table
    code_table = []
    for i in range (len(sequence)):
        #Take the last index of the sequence and concatenate it before the others
        coding = sequence[-1] + sequence[:-1]
        code_table.append(coding)
        #reattribute the swapped sequence to sequence
        sequence = coding
    #sort the BWT table
    bwt_table = sorted(code_table)

    #get the last character for each element of the sorted table to get the BWT code
    bwt_code = []
    for char in bwt_table:
        bwt_code.append(char[-1])
    #concatenate each character in the list to form a string and show the BWT
    bwt_concatcode = ''.join(bwt_code)
    return BwtCodingResult(code_table, bwt_table, bwt_code, bwt_concatcode)

class BwtDecodingResult:
    '''This class retrieves from the decoding_BWT function, the recoding matrix
    and the original sequence'''
    def __init__(self, decode, decoded_seq):
        self.decode = decode
        self.decoded_seq = decoded_seq

def decoding_BWT(bwt_code):
    """This function recodes a sequence from a code generated with the BWT as an
    input and returns the recoding matrix and the original sequence"""
    decode = bwt_code
    #the external loop allows the sorting of the growing recoding table
    for i in range (len(bwt_code)-1):
        decode = sorted(decode)
        #the internal loop concatenates the BWT code in front of the sorted code and
        #appends it to the list containing the sorted code generated in the 1st loop
        for j in range(len(decode)):
            decode.append(bwt_code[j]+decode[j])
        #remove the intermediate sorted code from the table
        del decode[0:(len(bwt_code))]

    #Find the original sequence with the $ in last position from the decoding
    #matrix and remove the $ from it.
    for i in decode:
        if i[-1] == '$':
            decoded_seq = i[:-1]
    return BwtDecodingResult(decode, decoded_seq)



if __name__ == "__main__":
    sequence = input("enter your sequence: ")
    coding = coding_BWT(sequence)

    print("BWT coding table:")
    for i in range(len(coding.bwt_table)):
        print(coding.bwt_table[i])
    print("\nThe BWT is: " + coding.bwt_concatcode)
    decoding = decoding_BWT(coding.bwt_code)
    print("\nThe decoding BWT table is:")
    for i in range(len(decoding.decode)):
        print(decoding.decode[i])
    print("\nThe original sequence is: " + decoding.decoded_seq)
